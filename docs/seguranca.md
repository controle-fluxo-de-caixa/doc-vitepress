# Login

O acesso ao nosso sistema é concedido mediante a utilização de um endereço de e-mail e uma senha. A senha é processada por meio de criptografia com hash, o que assegura um nível mais elevado de segurança.

## Exemplo de Utilização de Hash MD5

Para assegurar a integridade e segurança das informações em nosso sistema de login, implementamos o uso do algoritmo de hash *MD5 (Message Digest Algorithm 5)*. Este algoritmo converte uma entrada de dados em uma representação única e fixa, conhecida como hash, que é armazenada em nossos bancos de dados. Vamos ilustrar esse processo com um exemplo prático:

**Input**

````txt
"Hello, world!"
````

Hash correspondente (MD5):

**Output**

```
5eb63bbbe01eeed093cb22bb8f5acdc3
```

Neste exemplo, a frase "Hello, world!" é submetida ao algoritmo MD5, resultando no hash "5eb63bbbe01eeed093cb22bb8f5acdc3". Esse valor hash é então utilizado para verificar a autenticidade da senha durante o processo de login.

Quando o usuário criar uma senha, ela é convertida em um hash MD5 e, em seguida, apenas o hash é armazenado em nossos sistemas, não a senha original. Durante o login, o sistema converte a senha digitada pelo usuário em um hash MD5 e compara-o com o hash armazenado em nosso banco de dados. Se os hashes coincidirem, o login é bem-sucedido, proporcionando uma camada adicional de segurança, pois a senha real nunca é armazenada em texto simples.

## Recuperar senha

O procedimento para recuperação de senha consiste em inserir o endereço de e-mail associado à conta. Posteriormente, o usuário receberá um link por e-mail, o qual permitirá a alteração da senha. 

Este link inclui um token que deve ser fornecido durante o processo de alteração, garantindo a verificação da autenticidade do usuário que está realizando a modificação da senha.

**Exemplo de link para alterar senha**

```txt
http://127.0.0.0:0000/reset_password
```

**Exemplo de Token**

```txt
f91610085f79c22c14cd09028b0ef9g6
```