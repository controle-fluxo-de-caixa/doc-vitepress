---
layout: home

hero:
  name: "Inspirar CF"
  text: "Sistema de controle financeiro"
  tagline: "Documentação do sistema"
  image:
    src: ../icons/logo.png
  actions:
    - theme: brand
      text: Começar
      link: /introducao/visao-geral
    - theme: alt
      text: Navegação
      link: /navegacao/estrutura-navegacao

features:
  - title: Controle de movimentação financeira
    details: É a chave para gerir com eficiência receitas, despesas e investimentos, proporcionando uma visão clara e estratégica das finanças
  - title: Dashboard interativo
    details:  oferece uma visão instantânea e personalizada do desempenho financeiro, simplificando a análise e tomada de decisões estratégicas
  - title: Controle de usuários
    details: Administre com precisão, atribua permissões e gerencie os usuários para um acesso seguro e personalizado ao sistema.
---

