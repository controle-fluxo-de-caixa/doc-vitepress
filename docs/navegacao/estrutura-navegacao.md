# Navegação

## Estrutura de Navegação

A estrutura de navegação do nosso sistema foi projetada para oferecer uma experiência de usuário intuitiva e fácil de usar. Os elementos de navegação estão organizados de maneira lógica, permitindo que os usuários acessem as diferentes funcionalidades com facilidade.

## Layout 

O layout apresenta uma barra de menu lateral à esquerda, contendo uma variedade de opções de navegação. Além disso, há um cabeçalho na parte superior, fornecendo informações adicionais e acesso ao perfil do usuário.

## Lógica de Navegação
A lógica de navegação é baseada na hierarquia das funcionalidades e nas necessidades dos usuários. Os menus são organizados de forma a facilitar a localização e o acesso rápido às áreas relevantes do sistema.

## Login e Redirecionamento
Após o login, os usuários são redirecionados para a tela principal do sistema, oferecendo uma visão geral e fácil acesso às funcionalidades disponíveis.

---
<br>

# Informações sobre Modo Mobile

## Comportamento no Modo Mobile

No modo mobile, o menu lateral é recolhido para proporcionar mais espaço na tela. Os usuários podem acessar o menu ao clicar no ícone de hambúrguer no canto superior esquerdo.

## Acesso às Funcionalidades

As funcionalidades permanecem acessíveis no modo mobile, com um clique no ícone de hambúrguer revelando o menu lateral, onde as opções podem ser selecionadas facilmente.