# Menus Laterais
Na lateral esquerda da tela, são apresentados os menus de navegação, fornecendo acesso às diferentes funcionalidades e seções disponíveis. No modo mobile, o menu lateral é recolhido e exibe um ícone de hambúrguer no canto superior esquerdo. Ao clicar no hambúrguer, o menu lateral é exibido na tela com todas as opções disponíveis.

## Dashboard
Acesso ao resumo financeiro e estatísticas da empresa.

## Movimentação Financeira

Permite o registro e gerenciamento de movimentações financeiras, incluindo gastos e ganhos.

## Cadastro de Usuários

Permite o cadastro e gerenciamento das informações dos usuários.

## Competições

Acesso às informações e administração das competições realizadas.

---

### Comportamento Interativo
Os usuários podem clicar nas opções do menu para acessar as funcionalidades correspondentes.