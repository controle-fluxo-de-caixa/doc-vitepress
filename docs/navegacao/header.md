# Header Padrão
Em todas as telas, há um header padronizado que proporciona uma navegação e identificação consistentes.

## Logo do Sistema

A logo do sistema está presente no canto esquerdo do cabeçalho, proporcionando uma identidade visual consistente e reconhecível.

## Nome da Tela

Localizado no centro do cabeçalho, exibe o nome da tela atual, auxiliando os usuários a se orientarem dentro do sistema.

## Saudação Personalizada e Opções de Perfil
No canto direito do cabeçalho, é exibida uma saudação personalizada, seguida de opções para visualizar o perfil do usuário e fazer logout.