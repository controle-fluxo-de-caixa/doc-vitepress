
# Funcionalidade: Recuperação de Senha

### Descrição:
Esta funcionalidade permite que os usuários solicitem a recuperação de suas senhas caso a tenham esquecido ou desejem alterá-las por motivos de segurança.

### Capturas de Tela:
[Inserir capturas de tela relevantes.]

### Passos para Utilização:
1. **Solicitar Recuperação de Senha:**
   - O usuário acessa a tela de login e clica na opção "Esqueceu a Senha?".
   - Insere o endereço de e-mail associado à sua conta e clica em "Enviar".

2. **Receber Link de Recuperação por E-mail:**
   - O sistema envia um e-mail ao usuário com um link seguro para a recuperação da senha.

3. **Alterar Senha:**
   - O usuário clica no link recebido por e-mail, sendo redirecionado a uma página segura para criar uma nova senha.
   - Insere a nova senha e confirma a alteração.

### Exemplos ou Casos de Uso:
- **Exemplo 1:**
  - O usuário esquece sua senha e segue os passos para recuperá-la, recebendo um e-mail de recuperação.

- **Exemplo 2:**
  - Um usuário deseja alterar sua senha por motivos de segurança; ele segue os passos para a recuperação e define uma nova senha.
