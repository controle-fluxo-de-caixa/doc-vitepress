# Documentação de Funcionalidades

Bem-vindo à documentação das funcionalidades do nosso sistema. Abaixo está uma visão geral das funcionalidades disponíveis:

1. [*Dashboard*](#dashboard)
   - Permite a visualização consolidada da movimentação financeira.

2. [*Movimentação Financeira*](#movimentacao-financeira)
   - Permite o registro de novas movimentações financeiras, incluindo data, tipo (gasto ou ganho), descrição e categoria.

2. [*Aula*](#aula)
   - Permite o registro de novas aulas, incluindo sua descrição e o seu valor, quanto ela custa.

4. [*Usuário*](#usuario)
   - Permite o registro de novos usuários, aluno ou funcionários, incluindo seu nome, data de nascimento, e-mail, entre outros.

5. [*Mesalidade*](#mensalidade)
   - Permite a visualização das mensalidades dos alunos.

6. [*Notificação*](#notificacao)
   - Permite a visualização das notificações das mensalidades futuras e passadas dos alunos.

7. [*Competição*](#competicao)
   - Permite o registro de novas competições, incluindo nome, data e localização.

8. [*Agenda*](#agenda)
   - Permite o registro de novos lembretes, incluindo descrição, data de início e fim, e selecionar a aula atrelada ao lembrete.

9. [*Recuperação de Senha*](#recuperacao-de-senha)
   - Detalha o processo de recuperação de senha para os usuários, incluindo as etapas necessárias para redefinir suas senhas.



## Dashboard {#dashboard}

#### Descrição:

Esta funcionalidade permite aos usuários visualizar a movimentação financeira de uma maneira consolidada, permitindo acompanhar saldos, gastos e ganhos ao longo do tempo.   

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-dashboard)



## Movimentação Financeira {#movimentacao-financeira}

#### Descrição:

Esta funcionalidade permite aos usuários registrar novas movimentações financeiras, incluindo informações essenciais, como data, tipo (gasto ou ganho), descrição e categoria.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-movimentacao)



## Aula {#aula}

#### Descrição:

Esta funcionalidade permite aos usuários registrar novas aulas, incluindo informações essenciais, como a descrição e o quanto ela custa.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-aula)



## Usuário {#usuario}

#### Descrição:

Esta funcionalidade permite aos usuários registrar novos usuários, como alunos ou outros funcionários, incluindo informações essenciais, como o nome, data de nascimento, e-mail.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-usuario)



## Mensalidade {#mensalidade}

#### Descrição:

Esta funcionalidade permite aos usuários visualizar as mensalidades dos alunos.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-mensalidade)



## Notificação {#notificacao}

#### Descrição:

Esta funcionalidade permite aos usuários visualizar as notificações das mensalidades dos alunos.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-notificacao)



## Competição {#competicao}

#### Descrição:

Esta funcionalidade permite aos usuários registrar novas competições, incluindo informações essenciais, como nome, data e localização. Bem como adicionar participantes.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-competicao)



## Agenda {#agenda}

#### Descrição:

Esta funcionalidade permite aos usuários registrar novos lembretes, incluindo descrição, data de início, data final e selecionar a aula. Bem como visualizar um calendário que marca os dias que possuem lembrete.

#### [Ver Detalhes e Capturas de Tela](/funcionalidade/funcionalidade-agenda)



## Recuperação de Senha {#recuperacao-de-senha}

#### Descrição:

Detalha o processo de recuperação de senha para os usuários, incluindo as etapas necessárias para redefinir suas senhas.

#### [Ver Detalhes e Capturas de Tela](funcionalidade-recuperar-senha)



