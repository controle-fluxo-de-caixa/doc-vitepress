# Funcionalidade: Gestão de Usuários

### Descrição:
Esta funcionalidade permite aos administradores do sistema gerenciar usuários, incluindo a criação, edição e exclusão de perfis. Ideal para controlar o acesso e informações dos usuários no sistema.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/usuario/usuario-menu.jpeg)

![Exemplo de Captura de Tela](/assets/usuario/usuario-adicionar.jpeg)

![Exemplo de Captura de Tela](/assets/usuario/usuario-exclusao.jpeg)

### Passos para Utilização:

1. **Acessar a Página de Usuários:**
   - No menu lateral, selecione a opção "Usuários".

2. **Criar Novo Usuário:**
   - Clique no botão "+ Usuário" para abrir um modal de criação.
   - Preencha os campos obrigatórios, como nome, telefone, data de nascimento, e-mail e tipo de usuário.
   - Para usuários do tipo "Aluno", preencha informações adicionais, como selecionar uma aula para matricular e o nome do responsável.
   - Clique em "Salvar" para criar o novo usuário.

3. **Excluir Usuário Existente:**
   - Na lista de usuários, encontre o usuário que deseja excluir.
   - Ao final do usuário, clique no ícone de lixeira.
   - Uma mensagem de confirmação será exibida; clique em "Confirmar" para excluir o usuário ou "Cancelar" para abortar a ação.

### Exemplos ou Casos de Uso:

- **Exemplo 1: Criação de Novo Usuário:**
  - O administrador acessa a página de "Usuários", clica em "+ Usuário", preenche os detalhes do novo usuário e salva as informações.

- **Exemplo 2: Exclusão de Usuário:**
  - Um administrador decide remover um usuário do sistema, encontra o usuário na lista, clica no ícone de lixeira, confirma a exclusão e o usuário é removido.

### Notas Adicionais:

:::warning
- Certifique-se de categorizar corretamente os tipos de usuários para um gerenciamento eficiente.
- Não é possível editar um usuário, apenas ele mesmo pode editar suas informações.
:::