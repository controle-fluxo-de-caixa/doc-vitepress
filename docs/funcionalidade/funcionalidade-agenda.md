# Funcionalidade: Agenda

### Descrição:
A funcionalidade de Agenda permite aos usuários gerenciar lembretes associados a aulas, exibindo-os em um calendário mensal. Os lembretes são representados por pontos no calendário, e é possível adicionar, editar e excluir lembretes conforme necessário.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/agenda/agenda-menu.jpeg)

![Exemplo de Captura de Tela](/assets/agenda/agenda-adicionar.jpeg)

![Exemplo de Captura de Tela](/assets/agenda/agenda-editar-excluir.jpeg)

### Passos para Utilização:
1. **Acesso à Agenda:**
   - Clique no menu lateral chamado "Agenda" para acessar a tela da Agenda.

2. **Visualização do Calendário:**
   - Na tela da Agenda, você verá um calendário com o mês atual destacado.

3. **Adição de Novo Lembrete:**
   - Clique no botão "+ Lembrete".
   - Preencha os campos obrigatórios:
     - Descrição do lembrete.
     - Data inicial.
     - Data final (opcional).
     - Selecione a aula relacionada ao lembrete.
   - Confirme a adição do lembrete.

4. **Edição de Lembrete:**
   - No card listando os lembretes, clique no botão em formato de lápis para editar.
   - Faça as alterações necessárias nos campos.
   - Confirme as alterações.

5. **Exclusão de Lembrete:**
   - No card listando os lembretes, clique no ícone de lixeira.
   - Um modal será exibido, confirmando se você deseja realmente excluir o lembrete.
   - Confirme a exclusão.

### Exemplos ou Casos de Uso:
- **Exemplo 1: Adição de Lembrete Único:**
  - O usuário cria um lembrete para uma aula específica sem data final.

- **Exemplo 2: Adição de Lembrete com Período:**
  - O usuário cria um lembrete para uma sequência de aulas, definindo datas iniciais e finais.

### Notas Adicionais:
:::warning
- A edição e exclusão de lembretes são irreversíveis; verifique cuidadosamente antes de confirmar.
:::