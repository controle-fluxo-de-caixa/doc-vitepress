# Funcionalidade: Controle de Mensalidades

### Descrição:
Esta funcionalidade permite o controle eficiente das mensalidades dos alunos, categorizando-as em "Pendentes" e "Atrasadas". Ideal para monitorar e gerenciar o pagamento mensal dos alunos.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/mensalidade/mensalidade-menu.jpeg)

![Exemplo de Captura de Tela](/assets/mensalidade/mensalidade-filtro.jpeg)

### Passos para Utilização:

1. **Acessar a Página de Mensalidades:**
   - No menu lateral, selecione a opção "Mensalidade".

2. **Filtrar Mensalidades Pendentes por Mês:**
   - Na coluna de "Pendentes", utilize o filtro por mês para visualizar as mensalidades pendentes de um período específico.

3. **Filtrar Alunos com Mensalidades Atrasadas:**
   - Na coluna de "Atrasadas", utilize o campo de filtro por nome para localizar alunos com mensalidades atrasadas.

4. **Aprovar Pagamento:**
   - Ao final da visualização detalhada, utilize o campo "Aprovar Pagamento" para confirmar que o pagamento da mensalidade foi realizado com sucesso.

### Exemplos ou Casos de Uso:

- **Exemplo 1: Aprovação de Pagamento:**
  - O funcionário acessa a lista de mensalidades pendentes, visualiza os detalhes de uma mensalidade específica e, ao confirmar o pagamento, utiliza o campo "Aprovar Pagamento".

- **Exemplo 2: Filtro por Aluno Atrasado:**
  - Um funcionário utiliza o campo de filtro na coluna "Atrasadas" para localizar rapidamente os alunos com mensalidades em atraso.

### Notas Adicionais:
:::warning
- Certifique-se de instruir os funcionários sobre o significado do campo "Aprovar Pagamento" e quando utilizá-lo.
:::