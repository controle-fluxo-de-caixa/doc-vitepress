# Funcionalidade: Movimentação Financeira

### Descrição:
Esta funcionalidade permite aos usuários registrar movimentações financeiras, como gastos ou ganhos, fornecendo detalhes essenciais como descrição, valor, data e categoria. Ideal para o controle e registro preciso das finanças.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/movimentacao/movimentacao-menu.jpeg)

![Exemplo de Captura de Tela](/assets/movimentacao/movimentacao-adicionar.jpeg)

![Exemplo de Captura de Tela](/assets/movimentacao/movimentacao-edicao-exclusao.jpeg)

### Passos para Utilização:

1. **Acessar a Página de Movimentações:**
   - No menu lateral, selecione a opção "Movimentações".

2. **Registrar Nova Movimentação:**
   - Clique no botão "+ Movimentação" para abrir um modal de criação.
   - Preencha os campos obrigatórios, como descrição, valor, data e categoria (conta, mensalidade, etc.).
   - Selecione o tipo de movimentação financeira (gasto ou ganho).
   - Clique em "Salvar" para registrar a movimentação.

3. **Editar Movimentação Existente:**
   - Na lista de movimentações, encontre a movimentação que deseja editar.
   - Ao final da movimentação, clique no ícone de lápis para abrir a modal de edição.
   - Faça as alterações necessárias e clique em "Salvar".

4. **Excluir Movimentação Existente:**
   - Na lista de movimentações, encontre a movimentação que deseja excluir.
   - Ao final da movimentação, clique no ícone de lixeira.
   - Uma mensagem de confirmação será exibida; clique em "Confirmar" para excluir a movimentação ou "Cancelar" para abortar a ação.

### Exemplos ou Casos de Uso:

- **Exemplo 1: Registro de Gasto Mensal:**
  - O usuário acessa a página de "Movimentações", clica em "+ Movimentação", insere os detalhes de um gasto mensal e salva as informações.

- **Exemplo 2: Edição de Ganhos Recorrentes:**
  - Um usuário localiza uma entrada de ganhos recorrentes na lista de movimentações, clica no ícone de lápis, faz ajustes na descrição e valor, e salva as alterações.

- **Exemplo 3: Exclusão de Movimentação:**
  - Um usuário decide excluir uma movimentação específica, localiza a entrada na lista, clica no ícone de lixeira, confirma a exclusão e a movimentação é removida.

### Notas Adicionais:
- Categorias podem ser personalizadas conforme a necessidade da instituição.

:::warning
- Certifique-se de categorizar corretamente as movimentações para uma organização eficiente.
- Certifique-se de inserir informações precisas e corretas ao registrar uma movimentação financeira.
:::
