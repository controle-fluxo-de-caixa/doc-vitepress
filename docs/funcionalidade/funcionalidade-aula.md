# Funcionalidade: Aula

### Descrição:
Esta funcionalidade permite aos usuários criar, editar e excluir aulas. Ideal para professores, instrutores ou profissionais que desejam oferecer conteúdo educacional ou informativo em troca de um valor determinado.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/aula/aula-menu.jpeg) 

![Exemplo de Captura de Tela](/assets/aula/aula-adicionar.jpeg) 

![Exemplo de Captura de Tela](/assets/aula/aula-edicao-exclusao.jpeg) 

### Passos para Utilização:

1. **Acessar a Página de Aulas:**
   - No menu lateral, selecione a opção "Aulas".

2. **Criar Nova Aula:**
   - Clique no botão "+ Aula" para iniciar o processo de criação.
   - Insira um nome para a aula.
   - Especifique o valor associado à aula.
   - Clique em "Salvar" para criar a aula.

3. **Editar Aula Existente:**
   - Na lista de aulas, encontre a aula que deseja editar.
   - Ao final da aula, clique no ícone de lápis para abrir a modal de edição.
   - Faça as alterações desejadas e clique em "Salvar".

4. **Excluir Aula Existente:**
   - Na lista de aulas, encontre a aula que deseja excluir.
   - Ao final da aula, clique no ícone de lixeira.
   - Uma mensagem de confirmação será exibida; clique em "Confirmar" para excluir a aula ou "Cancelar" para abortar a ação.

### Exemplos ou Casos de Uso:

- **Exemplo 1: Criação de Aula para Curso Online:**
  - Um instrutor acessa a página "Aula" no menu lateral, clica em "+ Aula", insere o nome da aula e o valor, e em seguida, salva a criação.

- **Exemplo 2: Edição de Aula Existente:**
  - O mesmo instrutor decide atualizar as informações de uma aula existente, localiza a aula na lista, clica no ícone de lápis, faz as alterações necessárias e clica em "Salvar".

- **Exemplo 3: Exclusão de Aula:**
  - Um instrutor deseja remover uma aula específica, encontra a aula na lista, clica no ícone de lixeira, confirma a exclusão e a aula é removida.

### Notas Adicionais:
:::warning
- Certifique-se de fornecer informações claras e atrativas na descrição da aula para atrair potenciais interessados.
:::
