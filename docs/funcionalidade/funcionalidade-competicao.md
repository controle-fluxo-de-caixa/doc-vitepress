# Funcionalidade: Gestão de Competições

### Descrição:
A funcionalidade de "Competições" permite o gerenciamento eficaz de eventos esportivos, incluindo a criação, edição e exclusão de competições, bem como a adição de participantes.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/competicao/competicao-menu.jpeg)

![Exemplo de Captura de Tela](/assets/competicao/competicao-adicao.jpeg)

![Exemplo de Captura de Tela](/assets/competicao/competicao-adicao-integrante.jpeg)

![Exemplo de Captura de Tela](/assets/competicao/competicao-adicao-integrante-botao.jpeg)

### Passos para Utilização:

1. **Acessar a Página de Competições:**
   - No menu lateral, selecione a opção "Competições".

2. **Criar Nova Competição:**
   - Clique no botão "+ Competição" para abrir um modal.
   - Preencha os campos obrigatórios, como nome, data e localização da competição.
   - Clique em "Salvar" para criar a nova competição.

3. **Editar Competição Existente:**
   - Na lista de competições, encontre a competição que deseja editar.
   - Ao final da competição, clique no ícone de lápis para abrir a modal de edição.
   - Faça as alterações necessárias e clique em "Salvar".

4. **Excluir Competição Existente:**
   - Na lista de competições, encontre a competição que deseja excluir.
   - Ao final da competição, clique no ícone de lixeira.
   - Uma mensagem de confirmação será exibida; clique em "Confirmar" para excluir a competição ou "Cancelar" para abortar a ação.

5. **Adicionar Participante à Competição:**
   - Na lista da competição desejada, clique no ícone de olho para abrir um modal detalhando a competição.
   - No modal, utilize o botão "+ Integrante" para adicionar participantes, preenchendo os campos necessários como aluno, colocação e modalidade.
   - Clique em "Salvar" para adicionar o integrante.

### Exemplos ou Casos de Uso:

- **Exemplo 1: Criação de Nova Competição:**
  - Um organizador de eventos acessa a página de "Competições", cria uma nova competição preenchendo os detalhes e salva as informações.

- **Exemplo 2: Edição de Informações da Competição:**
  - Um administrador encontra uma competição na lista, clica no ícone de lápis, faz ajustes nas informações e salva as alterações.

- **Exemplo 3: Adição de Participante:**
  - Durante a organização de uma competição, é necessário adicionar um novo participante. O organizador utiliza o botão "+ Integrante" e insere os detalhes relevantes.

### Notas Adicionais:
:::warning
- Certifique-se de informar claramente os requisitos para a criação de uma nova competição.
- Destaque a importância de revisar as informações antes de confirmar a exclusão de uma competição.
:::