# Funcionalidade: Notificações

### Descrição:
A funcionalidade de Notificações oferece aos usuários um local centralizado para visualizar e gerenciar notificações relacionadas a mensalidades. Cada notificação é categorizada por cor para indicar seu status: vermelha para mensalidades vencidas, laranja para mensalidades próximas ao vencimento e verde para mensalidades futuras. Os usuários têm a opção de excluir notificações conforme necessário.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/notificacao/notificacao-menu.jpeg)

![Exemplo de Captura de Tela](/assets/notificacao/notificacao-exclusao.jpeg)

### Passos para Utilização:
1. **Acessar Notificações:**
   - Clique no menu "Notificações" na barra de navegação.

2. **Visualizar Notificações:**
   - Observe a lista de notificações, cada uma destacada por cor (vermelha, laranja, verde) de acordo com o status.

3. **Excluir Notificação:**
   - Ao final de cada notificação, utilize o botão de exclusão para remover notificações indesejadas.

### Exemplos ou Casos de Uso:
- **Exemplo 1: Gerenciamento de Mensalidades Vencidas:**
  - Um usuário verifica o painel de Notificações para lidar com mensalidades vencidas, identificadas pela cor vermelha, e exclui aquelas que já foram pagas.

- **Exemplo 2: Alerta para Mensalidades Próximas:**
  - Um usuário nota as notificações laranjas indicando mensalidades prestes a vencer e decide realizar os pagamentos necessários.

### Notas Adicionais:
:::warning
- As cores das notificações facilitam a identificação rápida do status da mensalidade.
- Certifique-se de revisar regularmente as notificações para manter um controle eficaz das obrigações financeiras.
:::