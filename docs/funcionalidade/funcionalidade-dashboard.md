# Funcionalidade: Dashboard

### Descrição:
O Dashboard fornece uma visão consolidada da movimentação financeira, permitindo aos usuários acompanhar saldos, gastos e ganhos ao longo do tempo. A funcionalidade também inclui gráficos de linha mensais, gráficos de barra anuais e gráficos de pizza para uma compreensão abrangente das finanças.

### Capturas de Tela:
![Exemplo de Captura de Tela](/assets/dashboard/dashboard-menu.jpeg)

![Exemplo de Captura de Tela](/assets/dashboard/dashboard-atualizar.jpeg)

![Exemplo de Captura de Tela](/assets/dashboard/dashboard-filtros.jpeg)

### Passos para Utilização:
1. **Acessar o Dashboard:**
   - Clique no menu "Meu Dashboard" na barra de navegação.

2. **Atualizar Valores:**
   - No Dashboard, clique no botão "Atualizar" para recalcular e exibir os valores mais recentes.

3. **Gráfico de Linha Mensal:**
   - Selecione um mês no filtro para visualizar o gráfico de linha, mostrando a movimentação financeira com linhas para saldo, gasto e ganho.

4. **Gráfico de Barra Anual:**
   - Utilize o filtro por ano para ver o gráfico de barra representando a diferença entre ganho e gasto para cada ano.

5. **Gráfico de Pizza por Categoria:**
   - Explore o gráfico de pizza para visualizar a divisão de gastos entre diferentes categorias.

### Exemplos ou Casos de Uso:
- **Exemplo 1: Acompanhamento Mensal:**
  - Um usuário verifica o Dashboard para acompanhar a movimentação financeira do mês selecionado, usando o gráfico de linha para entender melhor os padrões.

- **Exemplo 2: Análise Anual:**
  - Um usuário ajusta o filtro para um ano específico e examina o gráfico de barra para avaliar a diferença entre ganhos e gastos ao longo do ano.

### Notas Adicionais:
:::warning
- Certifique-se de que os dados estejam atualizados antes de analisar o Dashboard.
- Os gráficos oferecem uma visão intuitiva da situação financeira, facilitando a interpretação.
:::
