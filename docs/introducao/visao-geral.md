# Visão geral

A Inspirar CF é sistema de gerenciamento financeiro destinado ao controle e administração de uma associação voltada para atividades educacionais e esportivas. Sua finalidade principal é proporcionar uma gestão eficiente das finanças, registros de aulas, movimentações financeiras, controle de usuários e gerenciamento de competições. As funcionalidades-chave do sistema incluem:

## Cadastro e Gerenciamento de Aulas
Permite o cadastro detalhado das aulas oferecidas, facilitando o acompanhamento e organização da grade de horários.

## Movimentação Financeira
 Possibilita o registro das receitas e despesas, como mensalidades dos alunos e outros gastos relacionados à empresa.

## Cadastro de Alunos
Facilita o gerenciamento de informações dos alunos matriculados, incluindo dados pessoais e histórico de participação.

## Gerenciamento de Competições
Permite o registro e organização de competições esportivas, integrando informações relevantes sobre datas, participantes e resultados.

## Dashboard Financeiro
Oferece uma visão geral da situação financeira da instituição por meio de gráficos e métricas, fornecendo insights importantes para tomada de decisões.

## Controle de Usuários e Permissões
Possibilita o cadastro de usuários com diferentes níveis de acesso, restringindo o acesso a determinados menus de acordo com as permissões atribuídas a cada cargo.

Essa visão geral ilustra como o sistema visa fornecer uma solução abrangente para a gestão financeira e administrativa, promovendo eficiência e organização no ambiente educacional e esportivo.