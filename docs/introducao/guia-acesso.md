# Guia de Acesso e Utilização

O nosso sistema está hospedado em um ambiente online, eliminando a necessidade de instalação ou configuração local. Para acessar e utilizar o sistema, siga os passos abaixo:

## Acesso ao Sistema
Abra o seu navegador da web e acesse o site onde o sistema está hospedado. O endereço será fornecido pela administração da instituição.

## Página de Login
Na página inicial, clique em "Login" para acessar o sistema.

## Credenciais de Acesso
Insira seu e-mail e senha previamente fornecidos pela administração da instituição.

## Explorando o Sistema
Após o login bem-sucedido, explore as diversas funcionalidades do sistema acessando os menus disponíveis conforme suas permissões.

## Gerenciamento de Dados
Utilize os menus correspondentes para cadastrar aulas, movimentação financeira, alunos e competições, conforme necessário.

::: warning 
Lembramos que, dependendo do seu cargo, alguns menus podem não estar visíveis, pois as permissões de acesso são gerenciadas de acordo com a função de cada usuário.
:::
