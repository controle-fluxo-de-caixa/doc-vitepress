import { defineConfig } from 'vitepress'
import './style/index.css'

export default defineConfig({
  lang: 'pt-BR',
  title: "Inspirar CF",
  description: "Sistema de controle financeiro",
  head: [['link', { rel: 'icon', href: '../assets/favicon.ico' }]],
  themeConfig: {
    logo: '/icons/logo.png',
    nav: [
      { text: 'Página inicial', link: '/' },
      { text: 'Começar', link: '/introducao/visao-geral' }
    ],
    
    sidebar: [
      {
        text: 'Introdução',
        items: [
          { text: 'Visão geral', link: '/introducao/visao-geral' },
          { text: 'Guia de acesso', link: '/introducao/guia-acesso' },
        ]
      },
      {
        text: 'Navegação',
        items: [
          { text: 'Estrutura de Navegação', link: '/navegacao/estrutura-navegacao' },
          { text: 'Menu Lateral', link: '/navegacao/menu-lateral' },
          { text: 'Header Padrão', link: '/navegacao/header' },
        ]
      },
      {
        text: 'Funcionalidade',
        items: [
          { text: 'Geral', link: '/funcionalidade/funcionalidade' },
          { text: 'Dashboard', link: '/funcionalidade/funcionalidade-dashboard' },
          { text: 'Movimentação Financeira', link: '/funcionalidade/funcionalidade-movimentacao' },
          { text: 'Aula', link: '/funcionalidade/funcionalidade-aula' },
          { text: 'Usuário', link: '/funcionalidade/funcionalidade-usuario' },
          { text: 'Mensalidade', link: '/funcionalidade/funcionalidade-mensalidade' },
          { text: 'Notificação', link: '/funcionalidade/funcionalidade-notificacao' },
          { text: 'Competição', link: '/funcionalidade/funcionalidade-competicao' },
          { text: 'Agenda', link: '/funcionalidade/funcionalidade-agenda' },
          { text: 'Recuperar Senha', link: '/funcionalidade/funcionalidade-recuperar-senha' },
        ]
      },
      {
        text: 'Segurança',
        items: [
          { text: 'Segurança', link: '/seguranca' },
        ]
      },
    ],

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/seu-usuario/seu-repositorio' }
    ],

    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2019-present Evan You'
    },
  
  }
})
